import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter, Route } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'

import Root from './components/Root'
import store from './store'

ReactDOM.render(
	<Provider store={ store }>
		<BrowserRouter basename="/learnable">
			<Route component={ Root }/>
		</BrowserRouter>
	</Provider>,
	document.getElementById('root'))
registerServiceWorker()