export const boardActions = {
	REORDER_COLUMN: 'REORDER_COLUMN',
	REORDER_TASK: 'REORDER_TASK',
}

export const reorderColumn = dnd => ({
	type: boardActions.REORDER_COLUMN,
	dnd
})

export const reorderTask = dnd => ({
	type: boardActions.REORDER_TASK,
	dnd
})