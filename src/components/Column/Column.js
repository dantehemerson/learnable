import React from 'react'
import styled from 'styled-components'
import { Droppable , Draggable } from 'react-beautiful-dnd'

import Task from '../Task'

const Container = styled.div`
	margin: 20px 10px;
	padding: 10px;
	width: 400px;
	background: pink;
	display: flex;
	flex-direction: column;
`

const Title = styled.h3`
`

const TaskList = styled.div`
	padding: 8px;
	transition: background-color 0.2s ease;
	background-color: ${ props => props.isDraggingOver ? 'lightgrey' : 'inherit' };
	flex-grow: 1;
	min-height: 100px;
`

class Column extends React.Component {
	render() {
		return (
			<Draggable
				draggableId={ this.props.column.id }
				index={ this.props.index }>
				{
					provided => (
						<Container
							{ ...provided.draggableProps }
							innerRef={ provided.innerRef }>
							<Title { ...provided.dragHandleProps }>
								Colum: { this.props.column.title }
							</Title>
							<Droppable
								droppableId={this.props.column.id }
								type='task'>
									{
										(provided, snapshot) => (
											<TaskList
												innerRef={ provided.innerRef }
												{ ...provided.droppableProps }
												isDraggingOver={ snapshot.isDraggingOver }
											>
												{
													this.props.tasks.map((task, keyIndex) => (
														<Task key={ task.id} task={task} index={ keyIndex} />
													))
												}
												{ provided.placeholder }
											</TaskList>
										)
									}
							</Droppable>
						</Container>
					)
				}
			</Draggable>
		)
	}
}

export default Column