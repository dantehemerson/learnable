import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'

import Column from '../Column'
import { reorderColumn, reorderTask } from '../../actions'


const Container = styled.div`
`
const Title = styled.h1`
`
const Description = styled.h3`
`
const ColumnList = styled.div`
	display: flex;
	border: 1px solid gray;
`
class Board extends React.Component {
	onDragStart = (start, provided) => {
		provided.announce(`You have lifted the stack in position ${ start.source.index + 1 }`)
	}

	onDragUpdate = (update, provided) => {
		const message = update.destination
			? `You have moved the task to position ${update.destination.index + 1}`
			: `You are currently not over a droppable area`

		provided.announce(message)
	}

	onDragEnd = (result, provided) => {
		const message = result.destination
			? `You have moved the task from position
				${result.source.index + 1} to ${result.destination.index + 1}`
			: `The task has been returned to its starting position of
				${result.source.index + 1}`
		provided.announce(message)
		const { destination, source, draggableId, type } = result
		if(!destination) {
			return
		}
		if(destination.droppableId === source.droppableId &&
			destination.index === source.index) {
			return
		}

		if(type === 'column') {
			this.props.dispatchReorderColumn({
				boardId: 'board-1',
				sourceIndex: source.index,
				destinationIndex: destination.index,
				draggableId
			})
			return
		}

		const home = this.props.columns[source.droppableId]
		const foreign = this.props.columns[destination.droppableId]

		this.props.dispatchReorderTask({
			boardId: 'board-1',
			sourceIndex: source.index,
			destinationIndex: destination.index,
			draggableId,
			home,
			foreign
		})
	}

	render() {
		//console.log(this.props)
		return (
			<Container>
				<Title>{ this.props.title }</Title>
				<Description>{ this.props.description }</Description>
				<DragDropContext
					onDragStart={ this.onDragStart }
					onDragUpdate={ this.onDragUpdate }
					onDragEnd={ this.onDragEnd}>
					<Droppable
						droppableId='all-columns'
						direction='horizontal'
						type='column'>
						{
							provided => (
									<ColumnList
										{ ...provided.droppableProps }
										innerRef={ provided.innerRef }>
										{
											this.props.columnOrder.map((columnId, index) => {
												const column = this.props.columns[columnId]
												const tasks = column.taskIds.map(taskId => this.props.tasks[taskId])
												return (
													<Column
														key={ column.id }
														column={ column }
														tasks={ tasks }
														index={ index }
													/>
												)
											})
										}
										{ provided.placeholder }
									</ColumnList>
							)
						}
					</Droppable>
				</DragDropContext>
			</Container>
		)
	}
}

const mapStateToProps = state => ({
	...state.boards['board-1']
})

const mapDispatchToProps = dispatch => ({
	dispatchReorderColumn: dnd => dispatch(reorderColumn(dnd)),
	dispatchReorderTask: dnd => dispatch(reorderTask(dnd))
})

export default  connect(
	mapStateToProps,
	mapDispatchToProps
)(Board)