import React from 'react'
import styled from 'styled-components'
import { Draggable } from 'react-beautiful-dnd'

const Container = styled.div`
	margin: 5px;
	padding: 10px;
	background: white;
	background-color: ${ props => (props.isDragging ? 'lightgreen' : 'white') };
`

const Title = styled.h3`
	margin: 0;
`

export default props => (
	<Draggable
		draggableId={ props.task.id }
		index={ props.index }>
		{
			(provided, snapshot) => (
				<Container
					{ ...provided.draggableProps }
					{ ...provided.dragHandleProps }
					innerRef={ provided.innerRef }
					isDragging={ snapshot.isDragging }
					aria-roledescription='Press space bar to lift the task'>
					<Title>{ props.task.content }</Title>
				</Container>
			)
		}
	</Draggable>
)
