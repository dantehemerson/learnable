import React from 'react'

import Board from '../Board'

class Root extends React.Component {
	render() {
		return (
			<Board />
		)
	}
}

export default Root