import { boardActions } from '../actions'

const initialState = {
	'board-1': {
		id: 'board-1',
		title: 'Tablero 1',
		description: 'Cosas que debo hacer mañana',
		tasks: {
		  'task-1': { id: 'task-1', content: '1.-Take out the garbage' },
		  'task-2': { id: 'task-2', content: '2.-Watch my favorite show' },
		  'task-3': { id: 'task-3', content: '3.-Charge my phone' },
		  'task-4': { id: 'task-4', content: '4.-Cook dinner' },
		  'task-5': { id: 'task-5', content: '5.-Go to Mall' },
		  'task-6': { id: 'task-6', content: '6.-Buy a Car' },
		  'task-7': { id: 'task-7', content: '7.-Move my hands' },
		  'task-8': { id: 'task-8', content: '8.-Try win in PS' },
		  'task-9': { id: 'task-9', content: '9.-Play Football' },
		  'task-10': { id: 'task-10', content: '10.-Eat Makis' },
		},
		columns: {
		  'column-1': {
		    id: 'column-1',
		    title: 'To do',
		    taskIds: ['task-1', 'task-2', 'task-3', 'task-4'],
		  },
		  'column-2': {
		    id: 'column-2',
		    title: 'Other things',
		    taskIds: [],
		  },
		  'column-3': {
		    id: 'column-3',
		    title: 'Finished',
		    taskIds: [],
		  }
		},
		columnOrder: ['column-1', 'column-2', 'column-3']
	}
}

export default (state = initialState, action) => {
	const { dnd } = action
	switch(action.type) {
		case boardActions.REORDER_COLUMN:
			 const newColumnOrder = Array.from(state[action.dnd.boardId].columnOrder)
			 newColumnOrder.splice(action.dnd.sourceIndex, 1)
			 newColumnOrder.splice(action.dnd.destinationIndex, 0, action.dnd.draggableId)
			return {
				...state,
				[action.dnd.boardId]: {
					...state[action.dnd.boardId],
					columnOrder: newColumnOrder
				}
			}
		case boardActions.REORDER_TASK:
			if(dnd.home === dnd.foreign) {
				const newTaskIds = Array.from(dnd.home.taskIds)
				newTaskIds.splice(dnd.sourceIndex, 1)
				newTaskIds.splice(dnd.destinationIndex, 0, dnd.draggableId)
				const obj = {
					...state,
					[action.dnd.boardId]: {
						...state[action.dnd.boardId],
						columns: {
							...state[action.dnd.boardId].columns,
							[dnd.home.id]: {
								...state[action.dnd.boardId].columns[dnd.home.id],
								taskIds: newTaskIds
							}
						}
					}
				}
				return obj
			}

			// Eliminando elemento de la columna
			const homeTaskIds = Array.from(dnd.home.taskIds)
			homeTaskIds.splice(dnd.sourceIndex, 1)
			const newHome = {
				...dnd.home,
				taskIds: homeTaskIds
			}

			// Colocando en la nueva columna
			const foreignTaskIds = Array.from(dnd.foreign.taskIds)
			foreignTaskIds.splice(dnd.destinationIndex, 0, dnd.draggableId)
			const newForeign = {
				...dnd.foreign,
				taskIds: foreignTaskIds
			}

			const newObj = {
				...state,
				[action.dnd.boardId]: {
					...state[action.dnd.boardId],
					columns: {
						...state[action.dnd.boardId].columns,
						[newHome.id]: newHome,
						[newForeign.id]: newForeign
					}
				}
			}
			return newObj

		default:
			return state
	}
}